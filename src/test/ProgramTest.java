package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.company.ParsingResults;
import com.company.Program;
import org.junit.Test;

import java.io.File;

public class ProgramTest {

    @Test
    public void parseOriginalXml() {
        Program program = new Program();
        ParsingResults results = program.parse("src/test/20200930_OB_573060_UZSZ.xml");
        assertEquals(1, results.getTowns().size());
        assertEquals(5, results.getTownAreas().size());
    }

    @Test
    public void parseLargerXml() {
        Program program = new Program();
        ParsingResults results = program.parse("src/test/20200930_OB_573060_UZSZ_larger.xml");
        assertEquals(2, results.getTowns().size());
        assertEquals(5, results.getTownAreas().size());
    }

    @Test
    public void unzipFile() {
        Program program = new Program();
        String zipFilePath = "src/test/task.xml.zip";
        String destDir = "src/test/unzipping";
        File dir = new File(destDir);
        if (!dir.exists()) dir.mkdirs();
        String pathToFile = "src/test/unzipping/20200930_OB_573060_UZSZ.xml";
        File xml = new File(pathToFile);
        if (xml.exists()) xml.delete();
        program.unzip(zipFilePath, destDir);
        File xml2 = new File(pathToFile);
        boolean exists = xml2.exists();
        assertTrue(exists);
    }


}