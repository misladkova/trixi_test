package com.company;

public class Town {

    private String code;
    private String name;

    public Town(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
