package com.company;

import java.util.List;

public class ParsingResults {

    private List<Town> towns;
    private List<TownArea> townAreas;

    public ParsingResults(List<Town> towns, List<TownArea> townAreas) {
        this.towns = towns;
        this.townAreas = townAreas;
    }

    public List<Town> getTowns() {
        return towns;
    }

    public List<TownArea> getTownAreas() {
        return townAreas;
    }
}
