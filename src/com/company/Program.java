package com.company;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Program {

    private final String DB_URL = "jdbc:mariadb://localhost:3307/test";

    private final String USER = "user1";
    private final String PASS = "password1";

    private void download(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
    }

    public void unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);
        // create output directory if it doesn't exist
        if (!dir.exists()) dir.mkdirs();
        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                System.out.println("Unzipping to " + newFile.getAbsolutePath());
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ParsingResults parse(String xml) {
        List<Town> towns = new ArrayList<>();
        List<TownArea> townAreas = new ArrayList<>();

        //parse
        try {
            File inputFile = new File(xml);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList townsList = doc.getElementsByTagName("vf:Obec");

            for (int i = 0; i < townsList.getLength(); i++) {
                Node nNode = townsList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) nNode;
                    String code = element.getElementsByTagName("obi:Kod").item(0).getTextContent();
                    String name = element.getElementsByTagName("obi:Nazev").item(0).getTextContent();

                    towns.add(new Town(code, name));
                }
            }

            NodeList townAreasList = doc.getElementsByTagName("vf:CastObce");

            for (int j = 0; j < townAreasList.getLength(); j++) {
                Node node = townAreasList.item(j);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) node;
                    String townAreaCode = e.getElementsByTagName("coi:Kod").item(0).getTextContent();
                    String townAreaName = e.getElementsByTagName("coi:Nazev").item(0).getTextContent();
                    String townCode = e.getElementsByTagName("obi:Kod").item(0).getTextContent();

                    townAreas.add(new TownArea(townAreaCode, townAreaName, townCode));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ParsingResults(towns, townAreas);
    }

    public void database(ParsingResults results) {

        List<Town> towns = results.getTowns();
        List<TownArea> townAreas = results.getTownAreas();

        Connection c = null;
        Statement s = null;
        try {
            //STEP 2: Register JDBC driver
            Class.forName("org.mariadb.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to a selected database...");
            c = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            //STEP 4: Execute a query
            System.out.println("Inserting records into the table...");
            s = c.createStatement();

            String tab1 = "INSERT INTO obec VALUES (?, ?)";
            PreparedStatement ps1 = c.prepareStatement(tab1);
            for (Town t : towns) {
                ps1.setInt(1, Integer.valueOf(t.getCode()));
                ps1.setString(2, t.getName());
                ps1.executeUpdate();
            }

            String tab2 = "INSERT INTO cast_obce VALUES (?, ?, ?)";
            PreparedStatement ps2 = c.prepareStatement(tab2);
            for (TownArea ta : townAreas) {
                ps2.setInt(1, Integer.valueOf(ta.getTownAreaCode()));
                ps2.setString(2, ta.getTownAreaName());
                ps2.setInt(3, Integer.valueOf(ta.getTownCode()));
                ps2.executeUpdate();
            }
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (s != null)
                    c.close();
            } catch (SQLException se) {
            }// do nothing
            try {
                if (c != null)
                    c.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void run() {

        //download
        String url = "https://vdp.cuzk.cz/vymenny_format/soucasna/20200930_OB_573060_UZSZ.xml.zip";
        String file = "task.xml.zip";
        try {
            download(url, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //unzip
        String zipFilePath = "task.xml.zip";
        String destDir = ".";
        unzip(zipFilePath, destDir);

        ParsingResults results = parse("20200930_OB_573060_UZSZ.xml");

        database(results);
    }
}



