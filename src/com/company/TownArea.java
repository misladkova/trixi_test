package com.company;

public class TownArea {

    private String townAreaCode;
    private String townAreaName;
    private String townCode;

    public TownArea(String townAreaCode, String townAreaName, String townCode) {
        this.townAreaCode = townAreaCode;
        this.townAreaName = townAreaName;
        this.townCode = townCode;
    }

    public String getTownAreaCode() {
        return townAreaCode;
    }

    public String getTownAreaName() {
        return townAreaName;
    }

    public String getTownCode() {
        return townCode;
    }
}
